// This program is supposed to be executed as cli service
// Execute as generate [number of names (defaults 1)] [minimum letter amount (defaults 4)] [maximum letter amount (defaults 9)]

const args = process.argv;
const n = parseInt(args[2] || 1),
	min = parseInt(args[3] || 4),
	max = parseInt(args[4] || 9);

console.log(require('.')(n, min, max).join(" "));