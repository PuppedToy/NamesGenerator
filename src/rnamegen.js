function generarNombres(numeroNombres, minimoLetras, maximoLetras) {
    if(minimoLetras > maximoLetras) {
      var auxLetras = minimoLetras;
      minimoLetras = maximoLetras;
      maximoLetras = auxLetras;
    }

    var numeroLetrasDelNombre, numeroConsonantes, numeroVocales;

    var idioma = "EN";
    var letra = [];
    var nombre = [];

    var nombreFinal;

    numeroLetrasDelNombre = Math.floor(Math.random()*(maximoLetras-minimoLetras)+minimoLetras);
 
    for(var contador2=1;contador2<=numeroNombres;contador2++) {
      letra =  [];
      letra[0]=primeraLetra();
      nombreFinal=letra[0];
      if (!esVocal(letra[0])) {
        numeroConsonantes = 1;
        numeroVocales = 0;
      }
      else {
        numeroConsonantes = 0;
        numeroVocales = 1;
      }
      
      for(var contador1=1;contador1<numeroLetrasDelNombre;contador1++) { // bucle que se activa tantas veces como letras halla en el nombre
        letra[contador1] = determinarLetraParejaNormal(letra[contador1-1], idioma);
        if ((numeroConsonantes>=2 && !esVocal(letra[contador1])) || (numeroVocales>=2 && esVocal(letra[contador1])) || (suenaMalPrincipio(letra[0],letra[1], idioma) && contador1<=1) || (contador1>=numeroLetrasDelNombre-1 && suenaMalFinal(letra[contador1-1],letra[contador1],idioma)) ) {
          contador1--;
        }
        else
        {
          if (!esVocal(letra[contador1])) {
            numeroConsonantes++;
            numeroVocales = 0;
          }
          else {
            numeroVocales++;
            numeroConsonantes = 0;
          }
          nombreFinal = nombreFinal + letra[contador1];
        }
        
      }
      nombre.push(nombreFinal);

      numeroLetrasDelNombre = Math.floor((Math.random()*(maximoLetras-minimoLetras)+minimoLetras));
    }
    return nombre;
  }

   function esVocal (caracter) { // funcion para comprobar si una letra es vocal
    return caracter.toLowerCase() == "a" || caracter.toLowerCase() == "e" || caracter.toLowerCase() == "i" || caracter.toLowerCase() == "o" || caracter.toLowerCase() == "u" || caracter.toLowerCase() == "w";
    }

function primeraLetra() { // Calculo cuál será la primera letra mediante la frecuencia de aparición de primeras letras en el inglés http://en.wikipedia.org/wiki/Letter_frequency#Relative_frequencies_of_the_first_letters_of_a_word_in_the_English_language
    var auxPrimeraLetra = Math.random()*100;
    if(auxPrimeraLetra>=0.000 && auxPrimeraLetra<=11.602) return "A";
    if(auxPrimeraLetra>11.602 && auxPrimeraLetra<=16.304) return "B";
    if(auxPrimeraLetra>16.304 && auxPrimeraLetra<=19.815) return "C";
    if(auxPrimeraLetra>19.815 && auxPrimeraLetra<=22.485) return "D";
    if(auxPrimeraLetra>22.485 && auxPrimeraLetra<=24.492) return "E";
    if(auxPrimeraLetra>24.492 && auxPrimeraLetra<=28.271) return "F";
    if(auxPrimeraLetra>28.271 && auxPrimeraLetra<=30.221) return "G";
    if(auxPrimeraLetra>30.221 && auxPrimeraLetra<=37.453) return "H";
    if(auxPrimeraLetra>37.453 && auxPrimeraLetra<=43.739) return "I";
    if(auxPrimeraLetra>43.739 && auxPrimeraLetra<=44.336) return "J";
    if(auxPrimeraLetra>44.336 && auxPrimeraLetra<=44.926) return "K";
    if(auxPrimeraLetra>44.926 && auxPrimeraLetra<=47.631) return "L";
    if(auxPrimeraLetra>47.631 && auxPrimeraLetra<=52.005) return "M";
    if(auxPrimeraLetra>52.005 && auxPrimeraLetra<=54.370) return "N";
    if(auxPrimeraLetra>54.370 && auxPrimeraLetra<=60.634) return "O";
    if(auxPrimeraLetra>60.634 && auxPrimeraLetra<=63.179) return "P";
    if(auxPrimeraLetra>63.179 && auxPrimeraLetra<=63.352) return "Q";
    if(auxPrimeraLetra>63.352 && auxPrimeraLetra<=65.005) return "R";
    if(auxPrimeraLetra>65.005 && auxPrimeraLetra<=72.760) return "S";
    if(auxPrimeraLetra>72.760 && auxPrimeraLetra<=89.431) return "T";
    if(auxPrimeraLetra>89.431 && auxPrimeraLetra<=90.918) return "U";
    if(auxPrimeraLetra>90.918 && auxPrimeraLetra<=91.567) return "V";
    if(auxPrimeraLetra>91.567 && auxPrimeraLetra<=98.320) return "W";
    if(auxPrimeraLetra>98.320 && auxPrimeraLetra<=98.337) return "X";
    if(auxPrimeraLetra>98.337 && auxPrimeraLetra<=99.957) return "Y";
    if(auxPrimeraLetra>99.957 && auxPrimeraLetra<=100.00) return "Z";
    return "";
  }

  function determinarLetraParejaNormal (letra, idioma) { // utilizo probabilidades por parejas: http://homepages.math.uic.edu/~leon/mcs425-s08/handouts/char_freq2.pdf
    var aux;
    switch (letra) {
      case "A":
      case "a":
        aux = Math.floor(Math.random()*823+1);
        if(aux>0 && aux<=1) return "a";
        if(aux>1 && aux<=21) return "b";
        if(aux>21 && aux<=54) return "c";
        if(aux>54 && aux<=106) return "d";
        if(aux>106 && aux<=106) return "e";
        if(aux>106 && aux<=118) return "f";
        if(aux>118 && aux<=136) return "g";
        if(aux>136 && aux<=141) return "h";
        if(aux>141 && aux<=180) return "i";
        if(aux>180 && aux<=181) return "j";
        if(aux>181 && aux<=193) return "k";
        if(aux>193 && aux<=250) return "l";
        if(aux>250 && aux<=276) return "m";
        if(aux>276 && aux<=457) return "n";
        if(aux>457 && aux<=458) return "o";
        if(aux>458 && aux<=478) return "p";
        if(aux>478 && aux<=479) return "q";
        if(aux>479 && aux<=554) return "r";
        if(aux>554 && aux<=649) return "s";
        if(aux>649 && aux<=753) return "t";
        if(aux>753 && aux<=762) return "u";
        if(aux>762 && aux<=782) return "v";
        if(aux>782 && aux<=795) return "w";
        if(aux>795 && aux<=796) return "x";
        if(aux>796 && aux<=822) return "y";
        if(aux>822 && aux<=823) return "z";
      case "B":
      case "b":
        aux = Math.floor(Math.random()*148+1);
        if(aux>0 && aux<=11) return "a";
        if(aux>11 && aux<=12) return "b";
        if(aux>12 && aux<=12) return "c";
        if(aux>12 && aux<=12) return "d";
        if(aux>12 && aux<=59) return "e";
        if(aux>59 && aux<=59) return "f";
        if(aux>59 && aux<=59) return "g";
        if(aux>59 && aux<=59) return "h";
        if(aux>59 && aux<=65) return "i";
        if(aux>65 && aux<=66) return "j";
        if(aux>66 && aux<=66) return "k";
        if(aux>66 && aux<=83) return "l";
        if(aux>83 && aux<=83) return "m";
        if(aux>83 && aux<=83) return "n";
        if(aux>83 && aux<=102) return "o";
        if(aux>102 && aux<=102) return "p";
        if(aux>102 && aux<=102) return "q";
        if(aux>102 && aux<=113) return "r";
        if(aux>113 && aux<=115) return "s";
        if(aux>115 && aux<=116) return "t";
        if(aux>116 && aux<=137) return "u";
        if(aux>137 && aux<=137) return "v";
        if(aux>137 && aux<=137) return "w";
        if(aux>137 && aux<=137) return "x";
        if(aux>137 && aux<=148) return "y";
        if(aux>148 && aux<=148) return "z";
      case "C":
      case "c":
        aux = Math.floor(Math.random()*229+1);
        if(aux>0 && aux<=31) return "a";
        if(aux>31 && aux<=31) return "b";
        if(aux>31 && aux<=35) return "c";
        if(aux>35 && aux<=35) return "d";
        if(aux>35 && aux<=73) return "e";
        if(aux>73 && aux<=73) return "f";
        if(aux>73 && aux<=73) return "g";
        if(aux>73 && aux<=111) return "h";
        if(aux>111 && aux<=121) return "i";
        if(aux>121 && aux<=121) return "j";
        if(aux>121 && aux<=139) return "k";
        if(aux>139 && aux<=148) return "l";
        if(aux>148 && aux<=148) return "m";
        if(aux>148 && aux<=148) return "n";
        if(aux>148 && aux<=193) return "o";
        if(aux>193 && aux<=193) return "p";
        if(aux>193 && aux<=194) return "q";
        if(aux>194 && aux<=205) return "r";
        if(aux>205 && aux<=206) return "s";
        if(aux>206 && aux<=221) return "t";
        if(aux>221 && aux<=228) return "u";
        if(aux>228 && aux<=228) return "v";
        if(aux>228 && aux<=228) return "w";
        if(aux>228 && aux<=228) return "x";
        if(aux>228 && aux<=229) return "y";
        if(aux>229 && aux<=229) return "z";
      case "D":
      case "d":
        aux = Math.floor(Math.random()*478+1);
        if(aux>0 && aux<=48) return "a";
        if(aux>48 && aux<=68) return "b";
        if(aux>68 && aux<=77) return "c";
        if(aux>77 && aux<=90) return "d";
        if(aux>90 && aux<=147) return "e";
        if(aux>147 && aux<=158) return "f";
        if(aux>158 && aux<=165) return "g";
        if(aux>165 && aux<=190) return "h";
        if(aux>190 && aux<=240) return "i";
        if(aux>240 && aux<=243) return "j";
        if(aux>243 && aux<=244) return "k";
        if(aux>244 && aux<=255) return "l";
        if(aux>255 && aux<=269) return "m";
        if(aux>269 && aux<=285) return "n";
        if(aux>285 && aux<=326) return "o";
        if(aux>326 && aux<=332) return "p";
        if(aux>332 && aux<=332) return "q";
        if(aux>332 && aux<=346) return "r";
        if(aux>346 && aux<=381) return "s";
        if(aux>381 && aux<=437) return "t";
        if(aux>437 && aux<=447) return "u";
        if(aux>447 && aux<=449) return "v";
        if(aux>449 && aux<=468) return "w";
        if(aux>468 && aux<=468) return "x";
        if(aux>468 && aux<=478) return "y";
        if(aux>478 && aux<=478) return "z";
      case "E":
      case "e":
        aux = Math.floor(Math.random()*1237+1);
        if(aux>0 && aux<=110) return "a";
        if(aux>110 && aux<=133) return "b";
        if(aux>133 && aux<=178) return "c";
        if(aux>178 && aux<=304) return "d";
        if(aux>304 && aux<=352) return "e";
        if(aux>352 && aux<=382) return "f";
        if(aux>382 && aux<=397) return "g";
        if(aux>397 && aux<=430) return "h";
        if(aux>430 && aux<=471) return "i";
        if(aux>471 && aux<=474) return "j";
        if(aux>474 && aux<=479) return "k";
        if(aux>479 && aux<=534) return "l";
        if(aux>534 && aux<=581) return "m";
        if(aux>581 && aux<=692) return "n";
        if(aux>692 && aux<=725) return "o";
        if(aux>725 && aux<=753) return "p";
        if(aux>753 && aux<=755) return "q";
        if(aux>755 && aux<=924) return "r";
        if(aux>924 && aux<=1039) return "s";
        if(aux>1039 && aux<=1122) return "t";
        if(aux>1122 && aux<=1128) return "u";
        if(aux>1128 && aux<=1152) return "v";
        if(aux>1152 && aux<=1202) return "w";
        if(aux>1202 && aux<=1211) return "x";
        if(aux>1211 && aux<=1237) return "y";
        if(aux>1237 && aux<=1237) return "z";          
      case "F":
      case "f":
        aux = Math.floor(Math.random()*223+1);
        if(aux>0 && aux<=25) return "a";
        if(aux>25 && aux<=27) return "b";
        if(aux>27 && aux<=30) return "c";
        if(aux>30 && aux<=32) return "d";
        if(aux>32 && aux<=52) return "e";
        if(aux>52 && aux<=63) return "f";
        if(aux>63 && aux<=64) return "g";
        if(aux>64 && aux<=72) return "h";
        if(aux>72 && aux<=95) return "i";
        if(aux>95 && aux<=96) return "j";
        if(aux>96 && aux<=96) return "k";
        if(aux>96 && aux<=104) return "l";
        if(aux>104 && aux<=109) return "m";
        if(aux>109 && aux<=110) return "n";
        if(aux>110 && aux<=150) return "o";
        if(aux>150 && aux<=152) return "p";
        if(aux>152 && aux<=152) return "q";
        if(aux>152 && aux<=168) return "r";
        if(aux>168 && aux<=173) return "s";
        if(aux>173 && aux<=210) return "t";
        if(aux>210 && aux<=218) return "u";
        if(aux>218 && aux<=218) return "v";
        if(aux>218 && aux<=221) return "w";
        if(aux>221 && aux<=221) return "x";
        if(aux>221 && aux<=223) return "y";
        if(aux>223 && aux<=223) return "z";
      case "G":
      case "g":
        aux = Math.floor(Math.random()*208+1);
        if(aux>0 && aux<=24) return "a";
        if(aux>24 && aux<=27) return "b";
        if(aux>27 && aux<=29) return "c";
        if(aux>29 && aux<=31) return "d";
        if(aux>31 && aux<=59) return "e";
        if(aux>59 && aux<=62) return "f";
        if(aux>62 && aux<=66) return "g";
        if(aux>66 && aux<=101) return "h";
        if(aux>101 && aux<=119) return "i";
        if(aux>119 && aux<=120) return "j";
        if(aux>120 && aux<=120) return "k";
        if(aux>120 && aux<=127) return "l";
        if(aux>127 && aux<=130) return "m";
        if(aux>130 && aux<=134) return "n";
        if(aux>134 && aux<=157) return "o";
        if(aux>157 && aux<=158) return "p";
        if(aux>158 && aux<=158) return "q";
        if(aux>158 && aux<=170) return "r";
        if(aux>170 && aux<=179) return "s";
        if(aux>179 && aux<=195) return "t";
        if(aux>195 && aux<=202) return "u";
        if(aux>202 && aux<=202) return "v";
        if(aux>202 && aux<=207) return "w";
        if(aux>207 && aux<=207) return "x";
        if(aux>207 && aux<=208) return "y";
        if(aux>208 && aux<=208) return "z";
      case "H":
      case "h":
        aux = Math.floor(Math.random()*664+1);
        if(aux>0 && aux<=114) return "a";
        if(aux>114 && aux<=116) return "b";
        if(aux>116 && aux<=118) return "c";
        if(aux>118 && aux<=119) return "d";
        if(aux>119 && aux<=421) return "e";
        if(aux>421 && aux<=423) return "f";
        if(aux>423 && aux<=424) return "g";
        if(aux>424 && aux<=430) return "h";
        if(aux>430 && aux<=527) return "i";
        if(aux>527 && aux<=527) return "j";
        if(aux>527 && aux<=527) return "k";
        if(aux>527 && aux<=529) return "l";
        if(aux>529 && aux<=532) return "m";
        if(aux>532 && aux<=533) return "n";
        if(aux>533 && aux<=582) return "o";
        if(aux>582 && aux<=583) return "p";
        if(aux>583 && aux<=583) return "q";
        if(aux>583 && aux<=591) return "r";
        if(aux>591 && aux<=596) return "s";
        if(aux>596 && aux<=628) return "t";
        if(aux>628 && aux<=636) return "u";
        if(aux>636 && aux<=636) return "v";
        if(aux>636 && aux<=640) return "w";
        if(aux>640 && aux<=640) return "x";
        if(aux>640 && aux<=644) return "y";
        if(aux>644 && aux<=644) return "z";
      case "I":
      case "i":
        aux = Math.floor(Math.random()*676+1);
        if(aux>0 && aux<=10) return "a";
        if(aux>10 && aux<=15) return "b";
        if(aux>15 && aux<=47) return "c";
        if(aux>47 && aux<=80) return "d";
        if(aux>80 && aux<=103) return "e";
        if(aux>103 && aux<=120) return "f";
        if(aux>120 && aux<=145) return "g";
        if(aux>145 && aux<=151) return "h";
        if(aux>151 && aux<=152) return "i";
        if(aux>152 && aux<=153) return "j";
        if(aux>153 && aux<=161) return "k";
        if(aux>161 && aux<=198) return "l";
        if(aux>198 && aux<=235) return "m";
        if(aux>235 && aux<=414) return "n";
        if(aux>414 && aux<=438) return "o";
        if(aux>438 && aux<=444) return "p";
        if(aux>444 && aux<=444) return "q";
        if(aux>444 && aux<=471) return "r";
        if(aux>471 && aux<=557) return "s";
        if(aux>557 && aux<=650) return "t";
        if(aux>650 && aux<=651) return "u";
        if(aux>651 && aux<=665) return "v";
        if(aux>665 && aux<=672) return "w";
        if(aux>672 && aux<=674) return "x";
        if(aux>674 && aux<=674) return "y";
        if(aux>674 && aux<=676) return "z";
      case "J":
      case "j":
        aux = Math.floor(Math.random()*18+1);
        if(aux>0 && aux<=2) return "a";
        if(aux>2 && aux<=4) return "e";
        if(aux>4 && aux<=7) return "i";
        if(aux>7 && aux<=10) return "o";
        if(aux>10 && aux<=18) return "u";
      case "K":
      case "k":
        aux = Math.floor(Math.random()*85+1);
        if(aux>0 && aux<=6) return "a";
        if(aux>6 && aux<=7) return "b";
        if(aux>7 && aux<=8) return "c";
        if(aux>8 && aux<=9) return "d";
        if(aux>9 && aux<=38) return "e";
        if(aux>38 && aux<=39) return "f";
        if(aux>39 && aux<=39) return "g";
        if(aux>39 && aux<=41) return "h";
        if(aux>41 && aux<=55) return "i";
        if(aux>55 && aux<=55) return "j";
        if(aux>55 && aux<=55) return "k";
        if(aux>55 && aux<=57) return "l";
        if(aux>57 && aux<=58) return "m";
        if(aux>58 && aux<=67) return "n";
        if(aux>67 && aux<=71) return "o";
        if(aux>71 && aux<=71) return "p";
        if(aux>71 && aux<=71) return "q";
        if(aux>71 && aux<=71) return "r";
        if(aux>71 && aux<=76) return "s";
        if(aux>76 && aux<=80) return "t";
        if(aux>80 && aux<=81) return "u";
        if(aux>81 && aux<=81) return "v";
        if(aux>81 && aux<=83) return "w";
        if(aux>83 && aux<=83) return "x";
        if(aux>83 && aux<=85) return "y";
        if(aux>85 && aux<=85) return "z";
      case "L":
      case "l":
        aux = Math.floor(Math.random()*391+1);
        if(aux>0 && aux<=40) return "a";
        if(aux>40 && aux<=43) return "b";
        if(aux>43 && aux<=45) return "c";
        if(aux>45 && aux<=81) return "d";
        if(aux>81 && aux<=145) return "e";
        if(aux>145 && aux<=155) return "f";
        if(aux>155 && aux<=156) return "g";
        if(aux>156 && aux<=160) return "h";
        if(aux>160 && aux<=207) return "i";
        if(aux>207 && aux<=207) return "j";
        if(aux>207 && aux<=210) return "k";
        if(aux>210 && aux<=266) return "l";
        if(aux>266 && aux<=270) return "m";
        if(aux>270 && aux<=272) return "n";
        if(aux>272 && aux<=313) return "o";
        if(aux>313 && aux<=316) return "p";
        if(aux>316 && aux<=316) return "q";
        if(aux>316 && aux<=318) return "r";
        if(aux>318 && aux<=329) return "s";
        if(aux>329 && aux<=344) return "t";
        if(aux>344 && aux<=352) return "u";
        if(aux>352 && aux<=355) return "v";
        if(aux>355 && aux<=360) return "w";
        if(aux>360 && aux<=360) return "x";
        if(aux>360 && aux<=391) return "y";
        if(aux>391 && aux<=391) return "z";
      case "M":
      case "m":
        aux = Math.floor(Math.random()*252+1);
        if(aux>0 && aux<=44) return "a";
        if(aux>44 && aux<=51) return "b";
        if(aux>51 && aux<=52) return "c";
        if(aux>52 && aux<=53) return "d";
        if(aux>53 && aux<=121) return "e";
        if(aux>121 && aux<=123) return "f";
        if(aux>123 && aux<=124) return "g";
        if(aux>124 && aux<=127) return "h";
        if(aux>127 && aux<=152) return "i";
        if(aux>152 && aux<=152) return "j";
        if(aux>152 && aux<=152) return "k";
        if(aux>152 && aux<=153) return "l";
        if(aux>153 && aux<=158) return "m";
        if(aux>158 && aux<=160) return "n";
        if(aux>160 && aux<=189) return "o";
        if(aux>189 && aux<=200) return "p";
        if(aux>200 && aux<=200) return "q";
        if(aux>200 && aux<=203) return "r";
        if(aux>203 && aux<=213) return "s";
        if(aux>213 && aux<=222) return "t";
        if(aux>222 && aux<=230) return "u";
        if(aux>230 && aux<=230) return "v";
        if(aux>230 && aux<=234) return "w";
        if(aux>234 && aux<=234) return "x";
        if(aux>234 && aux<=252) return "y";
        if(aux>252 && aux<=252) return "z";
      case "N":
      case "n":
        aux = Math.floor(Math.random()*705+1);
        if(aux>0 && aux<=40) return "a";
        if(aux>40 && aux<=47) return "b";
        if(aux>47 && aux<=72) return "c";
        if(aux>72 && aux<=218) return "d";
        if(aux>218 && aux<=284) return "e";
        if(aux>284 && aux<=292) return "f";
        if(aux>292 && aux<=384) return "g";
        if(aux>384 && aux<=400) return "h";
        if(aux>400 && aux<=433) return "i";
        if(aux>433 && aux<=435) return "j";
        if(aux>435 && aux<=443) return "k";
        if(aux>443 && aux<=452) return "l";
        if(aux>452 && aux<=459) return "m";
        if(aux>459 && aux<=467) return "n";
        if(aux>467 && aux<=527) return "o";
        if(aux>527 && aux<=531) return "p";
        if(aux>531 && aux<=532) return "q";
        if(aux>532 && aux<=535) return "r";
        if(aux>535 && aux<=568) return "s";
        if(aux>568 && aux<=674) return "t";
        if(aux>674 && aux<=680) return "u";
        if(aux>680 && aux<=682) return "v";
        if(aux>682 && aux<=694) return "w";
        if(aux>694 && aux<=694) return "x";
        if(aux>694 && aux<=705) return "y";
        if(aux>705 && aux<=705) return "z";
      case "O":
      case "o":
        aux = Math.floor(Math.random()*767+1);
        if(aux>0 && aux<=16) return "a";
        if(aux>16 && aux<=28) return "b";
        if(aux>28 && aux<=41) return "c";
        if(aux>41 && aux<=59) return "d";
        if(aux>59 && aux<=64) return "e";
        if(aux>64 && aux<=144) return "f";
        if(aux>144 && aux<=151) return "g";
        if(aux>151 && aux<=162) return "h";
        if(aux>162 && aux<=174) return "i";
        if(aux>174 && aux<=175) return "j";
        if(aux>175 && aux<=188) return "k";
        if(aux>188 && aux<=214) return "l";
        if(aux>214 && aux<=262) return "m";
        if(aux>262 && aux<=368) return "n";
        if(aux>368 && aux<=404) return "o";
        if(aux>404 && aux<=419) return "p";
        if(aux>419 && aux<=419) return "q";
        if(aux>419 && aux<=503) return "r";
        if(aux>503 && aux<=531) return "s";
        if(aux>531 && aux<=588) return "t";
        if(aux>588 && aux<=703) return "u";
        if(aux>703 && aux<=715) return "v";
        if(aux>715 && aux<=761) return "w";
        if(aux>761 && aux<=761) return "x";
        if(aux>761 && aux<=766) return "y";
        if(aux>766 && aux<=767) return "z";          
      case "P":
      case "p":
        aux = Math.floor(Math.random()*159+1);
        if(aux>0 && aux<=23) return "a";
        if(aux>23 && aux<=24) return "b";
        if(aux>24 && aux<=24) return "c";
        if(aux>24 && aux<=24) return "d";
        if(aux>24 && aux<=54) return "e";
        if(aux>54 && aux<=55) return "f";
        if(aux>55 && aux<=55) return "g";
        if(aux>55 && aux<=58) return "h";
        if(aux>58 && aux<=70) return "i";
        if(aux>70 && aux<=70) return "j";
        if(aux>70 && aux<=70) return "k";
        if(aux>70 && aux<=85) return "l";
        if(aux>85 && aux<=86) return "m";
        if(aux>86 && aux<=86) return "n";
        if(aux>86 && aux<=107) return "o";
        if(aux>107 && aux<=117) return "p";
        if(aux>117 && aux<=117) return "q";
        if(aux>117 && aux<=135) return "r";
        if(aux>135 && aux<=140) return "s";
        if(aux>140 && aux<=151) return "t";
        if(aux>151 && aux<=157) return "u";
        if(aux>157 && aux<=157) return "v";
        if(aux>157 && aux<=158) return "w";
        if(aux>158 && aux<=158) return "x";
        if(aux>158 && aux<=159) return "y";
        if(aux>159 && aux<=159) return "z";  
      case "Q":
      case "q":
        return "u";
      case "R":
      case "r":
        aux = Math.floor(Math.random()*551+1);
        if(aux>0 && aux<=50) return "a";
        if(aux>50 && aux<=57) return "b";
        if(aux>57 && aux<=67) return "c";
        if(aux>67 && aux<=87) return "d";
        if(aux>87 && aux<=220) return "e";
        if(aux>220 && aux<=228) return "f";
        if(aux>228 && aux<=238) return "g";
        if(aux>238 && aux<=250) return "h";
        if(aux>250 && aux<=300) return "i";
        if(aux>300 && aux<=301) return "j";
        if(aux>301 && aux<=309) return "k";
        if(aux>309 && aux<=319) return "l";
        if(aux>319 && aux<=333) return "m";
        if(aux>333 && aux<=349) return "n";
        if(aux>349 && aux<=404) return "o";
        if(aux>404 && aux<=410) return "p";
        if(aux>410 && aux<=410) return "q";
        if(aux>410 && aux<=424) return "r";
        if(aux>424 && aux<=461) return "s";
        if(aux>461 && aux<=503) return "t";
        if(aux>503 && aux<=515) return "u";
        if(aux>515 && aux<=519) return "v";
        if(aux>519 && aux<=530) return "w";
        if(aux>530 && aux<=530) return "x";
        if(aux>530 && aux<=551) return "y";
        if(aux>551 && aux<=551) return "z";  
      case "S":
      case "s":
        aux = Math.floor(Math.random()*618+1);
        if(aux>0 && aux<=67) return "a";
        if(aux>67 && aux<=78) return "b";
        if(aux>78 && aux<=95) return "c";
        if(aux>95 && aux<=102) return "d";
        if(aux>102 && aux<=176) return "e";
        if(aux>176 && aux<=187) return "f";
        if(aux>187 && aux<=191) return "g";
        if(aux>191 && aux<=241) return "h";
        if(aux>241 && aux<=290) return "i";
        if(aux>290 && aux<=292) return "j";
        if(aux>292 && aux<=298) return "k";
        if(aux>298 && aux<=311) return "l";
        if(aux>311 && aux<=323) return "m";
        if(aux>323 && aux<=333) return "n";
        if(aux>333 && aux<=390) return "o";
        if(aux>390 && aux<=410) return "p";
        if(aux>410 && aux<=412) return "q";
        if(aux>412 && aux<=416) return "r";
        if(aux>416 && aux<=459) return "s";
        if(aux>459 && aux<=568) return "t";
        if(aux>568 && aux<=588) return "u";
        if(aux>588 && aux<=590) return "v";
        if(aux>590 && aux<=614) return "w";
        if(aux>614 && aux<=614) return "x";
        if(aux>614 && aux<=618) return "y";
        if(aux>618 && aux<=618) return "z";  
      case "T":
      case "t":
        aux = Math.floor(Math.random()*920+1);
        if(aux>0 && aux<=59) return "a";
        if(aux>59 && aux<=69) return "b";
        if(aux>69 && aux<=80) return "c";
        if(aux>80 && aux<=87) return "d";
        if(aux>87 && aux<=162) return "e";
        if(aux>162 && aux<=171) return "f";
        if(aux>171 && aux<=174) return "g";
        if(aux>174 && aux<=504) return "h";
        if(aux>504 && aux<=580) return "i";
        if(aux>580 && aux<=581) return "j";
        if(aux>581 && aux<=583) return "k";
        if(aux>583 && aux<=600) return "l";
        if(aux>600 && aux<=611) return "m";
        if(aux>611 && aux<=618) return "n";
        if(aux>618 && aux<=733) return "o";
        if(aux>733 && aux<=737) return "p";
        if(aux>737 && aux<=737) return "q";
        if(aux>737 && aux<=765) return "r";
        if(aux>765 && aux<=799) return "s";
        if(aux>799 && aux<=855) return "t";
        if(aux>855 && aux<=872) return "u";
        if(aux>872 && aux<=873) return "v";
        if(aux>873 && aux<=904) return "w";
        if(aux>904 && aux<=904) return "x";
        if(aux>904 && aux<=920) return "y";
        if(aux>920 && aux<=920) return "z";            
      case "U":
      case "u":
        aux = Math.floor(Math.random()*290+1);
        if(aux>0 && aux<=7) return "a";
        if(aux>7 && aux<=12) return "b";
        if(aux>12 && aux<=24) return "c";
        if(aux>24 && aux<=31) return "d";
        if(aux>31 && aux<=38) return "e";
        if(aux>38 && aux<=40) return "f";
        if(aux>40 && aux<=54) return "g";
        if(aux>54 && aux<=56) return "h";
        if(aux>56 && aux<=64) return "i";
        if(aux>64 && aux<=64) return "j";
        if(aux>64 && aux<=65) return "k";
        if(aux>65 && aux<=99) return "l";
        if(aux>99 && aux<=107) return "m";
        if(aux>107 && aux<=143) return "n";
        if(aux>143 && aux<=144) return "o";
        if(aux>144 && aux<=160) return "p";
        if(aux>160 && aux<=160) return "q";
        if(aux>160 && aux<=204) return "r";
        if(aux>204 && aux<=239) return "s";
        if(aux>239 && aux<=287) return "t";
        if(aux>287 && aux<=287) return "u";
        if(aux>287 && aux<=287) return "v";
        if(aux>287 && aux<=289) return "w";
        if(aux>289 && aux<=289) return "x";
        if(aux>289 && aux<=290) return "y";
        if(aux>290 && aux<=290) return "z";  
      case "V":
      case "v":
        aux = Math.floor(Math.random()*86+1);
        if(aux>0 && aux<=5) return "a";
        if(aux>5 && aux<=70) return "e";
        if(aux>70 && aux<=81) return "i";
        if(aux>81 && aux<=85) return "o";
        if(aux>85 && aux<=86) return "y";
      case "W":
      case "w":
        aux = Math.floor(Math.random()*252+1);
        if(aux>0 && aux<=66) return "a";
        if(aux>66 && aux<=67) return "b";
        if(aux>67 && aux<=68) return "c";
        if(aux>68 && aux<=70) return "d";
        if(aux>70 && aux<=109) return "e";
        if(aux>109 && aux<=110) return "f";
        if(aux>110 && aux<=110) return "g";
        if(aux>110 && aux<=154) return "h";
        if(aux>154 && aux<=193) return "i";
        if(aux>193 && aux<=193) return "j";
        if(aux>193 && aux<=193) return "k";
        if(aux>193 && aux<=195) return "l";
        if(aux>195 && aux<=196) return "m";
        if(aux>196 && aux<=208) return "n";
        if(aux>208 && aux<=237) return "o";
        if(aux>237 && aux<=237) return "p";
        if(aux>237 && aux<=237) return "q";
        if(aux>237 && aux<=240) return "r";
        if(aux>240 && aux<=244) return "s";
        if(aux>244 && aux<=248) return "t";
        if(aux>248 && aux<=249) return "u";
        if(aux>249 && aux<=249) return "v";
        if(aux>249 && aux<=251) return "w";
        if(aux>251 && aux<=251) return "x";
        if(aux>251 && aux<=252) return "y";
        if(aux>252 && aux<=252) return "z"; 
      case "X":
      case "x":
        aux = Math.floor(Math.random()*12+1);
        if(aux>0 && aux<=1) return "a";
        if(aux>1 && aux<=3) return "c";
        if(aux>3 && aux<=4) return "e";
        if(aux>4 && aux<=6) return "i";
        if(aux>6 && aux<=9) return "p";
        if(aux>9 && aux<=12) return "t";
      case "Y":
      case "y":
        aux = Math.floor(Math.random()*195+1);
        if(aux>0 && aux<=18) return "a";
        if(aux>18 && aux<=25) return "b";
        if(aux>25 && aux<=31) return "c";
        if(aux>31 && aux<=37) return "d";
        if(aux>37 && aux<=51) return "e";
        if(aux>51 && aux<=58) return "f";
        if(aux>58 && aux<=61) return "g";
        if(aux>61 && aux<=71) return "h";
        if(aux>71 && aux<=82) return "i";
        if(aux>82 && aux<=83) return "j";
        if(aux>83 && aux<=84) return "k";
        if(aux>84 && aux<=88) return "l";
        if(aux>88 && aux<=94) return "m";
        if(aux>94 && aux<=97) return "n";
        if(aux>97 && aux<=133) return "o";
        if(aux>133 && aux<=137) return "p";
        if(aux>137 && aux<=137) return "q";
        if(aux>137 && aux<=140) return "r";
        if(aux>140 && aux<=159) return "s";
        if(aux>159 && aux<=179) return "t";
        if(aux>179 && aux<=180) return "u";
        if(aux>180 && aux<=181) return "v";
        if(aux>181 && aux<=193) return "w";
        if(aux>193 && aux<=193) return "x";
        if(aux>193 && aux<=195) return "y";
        if(aux>195 && aux<=195) return "z"; 
      case "Z":
      case "z":
        aux = Math.floor(Math.random()*5+1);
        if(aux>0 && aux<=1) return "a";
        if(aux>1 && aux<=4) return "e";
        if(aux>4 && aux<=5) return "i";
      default:
        return "";
    }
}

function suenaMalFinal (letra1, letra2, idioma) {
    switch (letra1) {
      case "a":
      case "A":
        switch(letra2) {
        case "a":
          return true;
        default:
          return false;
      }
      case "b":
      case "B":
        switch(letra2) {
        case "c":
          return true;
        case "d":
          return true;
        case "f":
          return true;
        case "g":
          return true;
        case "h":
          return true;
        case "j":
          return true;
        case "k":
          return true;
        case "l":
          return true;
        case "m":
          return true;
        case "n":
          return true;
        case "p":
          return true;
        case "q":
          return true;
        case "r":
          return true;
        case "t":
          return true;
        case "v":
          return true;
        case "w":
          return true;
        case "x":
          return true;
        default:
          return false;
      }
      case "c":
      case "C":
        switch(letra2) {
        case "b":
          return true;
        case "c":
          return true;
        case "d":
          return true;
        case "f":
          return true;
        case "g":
          return true;
        case "j":
          return true;
        case "l":
          return true;
        case "m":
          return true;
        case "n":
          return true;
        case "p":
          return true;
        case "q":
          return true;
        case "r":
          return true;
        case "v":
          return true;
        case "w":
          return true;
        case "x":
          return true;
        case "z":
          return true;
        default:
          return false;
      }
      case "d":
      case "D":
        switch(letra2) {
        case "b":
          return true;
        case "c":
          return true;
        case "f":
          return true;
        case "g":
          return true;
        case "h":
          return true;
        case "j":
          return true;
        case "k":
          return true;
        case "l":
          return true;
        case "m":
          return true;
        case "n":
          return true;
        case "p":
          return true;
        case "q":
          return true;
        case "r":
          return true;
        case "t":
          return true;
        case "v":
          return true;
        case "w":
          return true;
        case "x":
          return true;
        case "z":
          return true;
        default:
          return false;
      }
      case "e":
      case "E":
        switch(letra2) {
        default:
          return false;
      }
      case "f":
      case "F":
        switch(letra2) {
        case "b":
          return true;
        case "c":
          return true;
        case "d":
          return true;
        case "g":
          return true;
        case "h":
          return true;
        case "j":
          return true;
        case "l":
          return true;
        case "m":
          return true;
        case "n":
          return true;
        case "p":
          return true;
        case "q":
          return true;
        case "r":
          return true;
        case "v":
          return true;
        case "w":
          return true;
        case "x":
          return true;
        case "z":
          return true;
        default:
          return false;
      }
      case "g":
      case "G":
        switch(letra2) {
        case "b":
          return true;
        case "c":
          return true;
        case "d":
          return true;
        case "f":
          return true;
        case "j":
          return true;
        case "k":
          return true;
        case "l":
          return true;
        case "m":
          return true;
        case "n":
          return true;
        case "p":
          return true;
        case "q":
          return true;
        case "r":
          return true;
        case "t":
          return true;
        case "v":
          return true;
        case "w":
          return true;
        case "x":
          return true;
        default:
          return false;
      }
      case "h":
      case "H":
        switch(letra2) {
        case "b":
          return true;
        case "c":
          return true;
        case "d":
          return true;
        case "g":
          return true;
        case "j":
          return true;
        case "k":
          return true;
        case "l":
          return true;
        case "q":
          return true;
        case "t":
          return true;
        case "v":
          return true;
        case "w":
          return true;
        default:
          return false;
      }
      case "i":
      case "I":
        switch(letra2) {
        case "i":
          return true;
        case "y":
          return true;
        default:
          return false;
      }
      case "j":
      case "J":
        switch(letra2) {
        case "b":
          return true;
        case "c":
          return true;
        case "d":
          return true;
        case "f":
          return true;
        case "g":
          return true;
        case "h":
          return true;
        case "k":
          return true;
        case "l":
          return true;
        case "m":
          return true;
        case "n":
          return true;
        case "p":
          return true;
        case "q":
          return true;
        case "r":
          return true;
        case "s":
          return true;
        case "t":
          return true;
        case "v":
          return true;
        case "w":
          return true;
        case "x":
          return true;
        case "z":
          return true;
        default:
          return false;
      }
      case "k":
      case "K":
        switch(letra2) {
        case "b":
          return true;
        case "c":
          return true;
        case "d":
          return true;
        case "f":
          return true;
        case "g":
          return true;
        case "h":
          return true;
        case "j":
          return true;
        case "l":
          return true;
        case "m":
          return true;
        case "n":
          return true;
        case "p":
          return true;
        case "q":
          return true;
        case "r":
          return true;
        case "t":
          return true;
        case "v":
          return true;
        case "w":
          return true;
        case "x":
          return true;
        default:
          return false;
      }
      case "l":
      case "L":
        switch(letra2) {
        case "b":
          return true;
        case "c":
          return true;
        case "g":
          return true;
        case "j":
          return true;
        case "q":
          return true;
        case "r":
          return true;
        case "v":
          return true;
        case "w":
          return true;
        default:
          return false;
      }
      case "m":
      case "M":
        switch(letra2) {
        case "c":
          return true;
        case "f":
          return true;
        case "g":
          return true;
        case "h":
          return true;
        case "j":
          return true;
        case "l":
          return true;
        case "q":
          return true;
        case "r":
          return true;
        case "t":
          return true;
        case "v":
          return true;
        case "w":
          return true;
        case "x":
          return true;
        default:
          return false;
      }
      case "n":
      case "N":
        switch(letra2) {
        case "b":
          return true;
        case "c":
          return true;
        case "f":
          return true;
        case "g":
          return true;
        case "h":
          return true;
        case "j":
          return true;
        case "l":
          return true;
        case "m":
          return true;
        case "p":
          return true;
        case "q":
          return true;
        case "r":
          return true;
        case "v":
          return true;
        case "w":
          return true;
        case "x":
          return true;
        default:
          return false;
      }
      case "o":
      case "O":
        switch(letra2) {
        default:
          return false;
      }
      case "p":
      case "P":
        switch(letra2) {
        case "b":
          return true;
        case "c":
          return true;
        case "d":
          return true;
        case "f":
          return true;
        case "g":
          return true;
        case "j":
          return true;
        case "k":
          return true;
        case "l":
          return true;
        case "m":
          return true;
        case "n":
          return true;
        case "p":
          return true;
        case "q":
          return true;
        case "r":
          return true;
        case "t":
          return true;
        case "v":
          return true;
        case "w":
          return true;
        case "x":
          return true;
        default:
          return false;
      }
      case "q":
      case "Q":
        switch(letra2) {
        case "a":
          return true;
        case "b":
          return true;
        case "c":
          return true;
        case "d":
          return true;
        case "e":
          return true;
        case "f":
          return true;
        case "g":
          return true;
        case "h":
          return true;
        case "i":
          return true;
        case "j":
          return true;
        case "k":
          return true;
        case "l":
          return true;
        case "m":
          return true;
        case "n":
          return true;
        case "o":
          return true;
        case "p":
          return true;
        case "q":
          return true;
        case "r":
          return true;
        case "s":
          return true;
        case "t":
          return true;
        case "u":
          return true;
        case "v":
          return true;
        case "w":
          return true;
        case "x":
          return true;
        case "y":
          return true;
        case "z":
          return true;
        default:
          return false;
      }
      case "r":
      case "R":
        switch(letra2) {
        case "c":
          return true;
        case "j":
          return true;
        case "l":
          return true;
        case "q":
          return true;
        case "v":
          return true;
        case "w":
          return true;
        default:
          return false;
      }
      case "s":
      case "S":
        switch(letra2) {
        case "b":
          return true;
        case "c":
          return true;
        case "d":
          return true;
        case "f":
          return true;
        case "g":
          return true;
        case "j":
          return true;
        case "l":
          return true;
        case "n":
          return true;
        case "q":
          return true;
        case "r":
          return true;
        case "v":
          return true;
        case "w":
          return true;
        case "x":
          return true;
        case "z":
          return true;
        default:
          return false;
      }
      case "t":
      case "T":
        switch(letra2) {
        case "b":
          return true;
        case "c":
          return true;
        case "d":
          return true;
        case "f":
          return true;
        case "g":
          return true;
        case "j":
          return true;
        case "l":
          return true;
        case "m":
          return true;
        case "n":
          return true;
        case "p":
          return true;
        case "q":
          return true;
        case "r":
          return true;
        case "v":
          return true;
        case "w":
          return true;
        case "x":
          return true;
        default:
          return false;
      }
      case "u":
      case "U":
        switch(letra2) {
        case "u":
          return true;
        default:
          return false;
      }
      case "v":
      case "V":
        switch(letra2) {
        case "b":
          return true;
        case "c":
          return true;
        case "d":
          return true;
        case "f":
          return true;
        case "g":
          return true;
        case "h":
          return true;
        case "j":
          return true;
        case "k":
          return true;
        case "l":
          return true;
        case "m":
          return true;
        case "n":
          return true;
        case "p":
          return true;
        case "q":
          return true;
        case "r":
          return true;
        case "s":
          return true;
        case "t":
          return true;
        case "v":
          return true;
        case "w":
          return true;
        case "x":
          return true;
        case "z":
          return true;
        default:
          return false;
      }
      case "w":
      case "W":
        switch(letra2) {
        case "b":
          return true;
        case "c":
          return true;
        case "h":
          return true;
        case "j":
          return true;
        case "l":
          return true;
        case "u":
          return true;
        case "v":
          return true;
        case "w":
          return true;
        case "x":
          return true;
        case "y":
          return true;
        default:
          return false;
      }
      case "x":
      case "X":
        switch(letra2) {
        case "b":
          return true;
        case "c":
          return true;
        case "d":
          return true;
        case "f":
          return true;
        case "g":
          return true;
        case "h":
          return true;
        case "j":
          return true;
        case "k":
          return true;
        case "l":
          return true;
        case "m":
          return true;
        case "n":
          return true;
        case "p":
          return true;
        case "q":
          return true;
        case "r":
          return true;
        case "s":
          return true;
        case "t":
          return true;
        case "v":
          return true;
        case "w":
          return true;
        case "z":
          return true;
        default:
          return false;
      }
      case "y":
      case "Y":
        switch(letra2) {
        case "c":
          return true;
        case "g":
          return true;
        case "h":
          return true;
        case "j":
          return true;
        case "l":
          return true;
        case "q":
          return true;
        case "t":
          return true;
        case "w":
          return true;
        case "y":
          return true;
        default:
          return false;
      }
      case "z":
      case "Z":
        switch(letra2) {
        case "b":
          return true;
        case "c":
          return true;
        case "d":
          return true;
        case "f":
          return true;
        case "g":
          return true;
        case "j":
          return true;
        case "l":
          return true;
        case "m":
          return true;
        case "n":
          return true;
        case "p":
          return true;
        case "q":
          return true;
        case "r":
          return true;
        case "s":
          return true;
        case "t":
          return true;
        case "v":
          return true;
        case "w":
          return true;
        case "x":
          return true;
        default:
          return false;
      }
      default:
        return false;
    }
}


function suenaMalPrincipio (letra1, letra2, idioma) { // funcion que sirve para ver si dos letras suenan mal al principio del nombre
    switch (letra1) {
      case "A":
      case "a":
        switch (letra2) {
        case "a":
          return true;
        default:
          return false;
      }
      case "B":
      case "b":
        switch (letra2) {
        case "b":
        case "c":
        case "d":
        case "f":
        case "g":
        case "j":
        case "k":
        case "m":
        case "n":
        case "p":
        case "q":
        case "s":
        case "t":
        case "v":
        case "w":
        case "x":
        case "z":
          return true;
        default:
          return false;
      }
      case "C":
      case "c":
        switch (letra2) {
        case "c":
        case "b":
        case "d":
        case "f":
        case "g":
        case "j":
        case "k":
        case "m":
        case "n":
        case "p":
        case "q":
        case "s":
        case "t":
        case "v":
        case "w":
        case "x":
        case "z":
          return true;
        default:
          return false;  
      }
      case "D":
      case "d":
        switch (letra2) {
        case "d":
        case "c":
        case "b":
        case "f":
        case "g":
        case "j":
        case "k":
        case "l":
        case "m":
        case "n":
        case "p":
        case "q":
        case "s":
        case "t":
        case "v":
        case "w":
        case "x":
        case "z":
          return true;
        default:
          return false;   
      }
      case "E":
      case "e":
        switch (letra2) {
        case "e":
          return true;
        default:
          return false;
      }
      case "F":
      case "f":
        switch (letra2) {
        case "f":
        case "c":
        case "d":
        case "b":
        case "g":
        case "j":
        case "k":
        case "m":
        case "n":
        case "p":
        case "q":
        case "s":
        case "t":
        case "v":
        case "w":
        case "x":
        case "z":
          return true;
        default:
          return false;  
      }
      case "G":
      case "g":
        switch (letra2) {
        case "g":
        case "c":
        case "d":
        case "f":
        case "b":
        case "j":
        case "k":
        case "m":
        case "p":
        case "q":
        case "s":
        case "t":
        case "v":
        case "w":
        case "x":
        case "z":
          return true;
        default:
          return false;  
      }
      case "H":
      case "h":
        switch (letra2) {
        case "h":
        case "b":
        case "c":
        case "d":
        case "f":
        case "g":
        case "j":
        case "k":
        case "l":
        case "m":
        case "n":
        case "p":
        case "q":
        case "r":
        case "s":
        case "t":
        case "v":
        case "w":
        case "x":
        case "z":
          return true;
        default:
          return false;  
      }
      case "I":
      case "i":
        switch(letra2) {
        case "i":
        case "e":
        case "o":
          return true;
        default:
          return false;  
      }
      case "J":
      case "j":
        switch (letra2) {
        case "j":
        case "c":
        case "d":
        case "f":
        case "g":
        case "b":
        case "k":
        case "l":
        case "m":
        case "n":
        case "p":
        case "q":
        case "r":
        case "s":
        case "t":
        case "v":
        case "w":
        case "x":
        case "z":
          return true;
        default:
          return false;  
      }
      case "K":
      case "k":
        switch (letra2) {
        case "k":
        case "c":
        case "d":
        case "f":
        case "g":
        case "j":
        case "b":
        case "m":
        case "p":
        case "q":
        case "s":
        case "t":
        case "v":
        case "w":
        case "x":
        case "z":
          return true;
        default:
          return false;  
      }
      case "L":
      case "l":
        switch (letra2) {
        case "l":
        case "b":
        case "c":
        case "d":
        case "f":
        case "g":
        case "j":
        case "k":
        case "m":
        case "n":
        case "p":
        case "q":
        case "r":
        case "s":
        case "t":
        case "v":
        case "w":
        case "x":
        case "z":
          return true;
        default:
          return false;  
      }
      case "M":
      case "m":
        switch (letra2) {
        case "m":
        case "c":
        case "d":
        case "f":
        case "g":
        case "j":
        case "k":
        case "l":
        case "b":
        case "n":
        case "p":
        case "q":
        case "r":
        case "s":
        case "t":
        case "v":
        case "w":
        case "x":
        case "z":
          return true;
        default:
          return false;  
      }
      case "N":
      case "n":
        switch (letra2) {
        case "n":
        case "c":
        case "d":
        case "f":
        case "g":
        case "j":
        case "k":
        case "l":
        case "m":
        case "b":
        case "p":
        case "q":
        case "r":
        case "s":
        case "t":
        case "v":
        case "w":
        case "x":
        case "z":
          return true;
        default:
          return false;  
      }
      case "O":
      case "o":
        switch (letra2) {
        case "o":
        case "a":
        case "e":
        case "i":
        case "u":
        case "w":
          return true;
        default:
          return false;  
      }
      case "P":
      case "p":
        switch (letra2) {
        case "p":
        case "c":
        case "d":
        case "f":
        case "g":
        case "j":
        case "k":
        case "m":
        case "n":
        case "b":
        case "q":
        case "s":
        case "t":
        case "v":
        case "x":
        case "w":
        case "z":
          return true;
        default:
          return false;  
      }
      case "Q":
      case "q":
        switch (letra2) {
        case "q":
          return true;
        default:
          return false;
      }
      case "R":
      case "r":
        switch (letra2) {
        case "r":
        case "c":
        case "d":
        case "f":
        case "g":
        case "j":
        case "k":
        case "l":
        case "m":
        case "b":
        case "p":
        case "q":
        case "n":
        case "s":
        case "t":
        case "v":
        case "w":
        case "x":
        case "z":
          return true;
        default:
          return false;  
      }
      case "S":
      case "s":
        switch (letra2) {
        case "s":
        case "d":
        case "f":
        case "g":
        case "j":
        case "m":
        case "b":
        case "q":
        case "r":
        case "x":
        case "z":
          return true;
        default:
          return false;  
      }
      case "T":
      case "t":
        switch (letra2) {
        case "t":
        case "c":
        case "d":
        case "f":
        case "g":
        case "j":
        case "k":
        case "l":
        case "m":
        case "n":
        case "b":
        case "p":
        case "q":
        case "s":
        case "v":
        case "w":
        case "x":
        case "z":
          return true;
        default:
          return false;  
      }
      case "U":
      case "u":
        switch (letra2) {
        case "a":
        case "e":
        case "i":
        case "o":
        case "u":
        case "w":
          return true;
        default:
          return false;  
      }
      case "V":
      case "v":
        switch (letra2) {
        case "v":
        case "c":
        case "d":
        case "f":
        case "g":
        case "j":
        case "k":
        case "l":
        case "m":
        case "n":
        case "p":
        case "q":
        case "r":
        case "s":
        case "t":
        case "b":
        case "w":
        case "x":
        case "z":
          return true;
        default:
          return false;  
      }
      case "W":
      case "w":
        switch (letra2) {
        case "w":
          return true;
        default:
          return false;
      }
      case "X":
      case "x":
        switch (letra2) {
        case "x":
        case "c":
        case "d":
        case "f":
        case "g":
        case "j":
        case "k":
        case "l":
        case "m":
        case "n":
        case "p":
        case "q":
        case "r":
        case "s":
        case "t":
        case "v":
        case "w":
        case "b":
        case "z":
          return true;
        default:
          return false;  
      }
      case "Y":
      case "y":
        switch (letra2) {
        case "y":
        case "w":
          return true;
        default:
          return false;
      }
      case "Z":
      case "z":
        switch (letra2) {
        case "z":
        case "c":
        case "d":
        case "f":
        case "g":
        case "j":
        case "k":
        case "l":
        case "m":
        case "n":
        case "p":
        case "q":
        case "s":
        case "t":
        case "v":
        case "w":
        case "x":
        case "b":
          return true;
        default:
          return false;  
      }
      default:
        return false;
    }
}

if(typeof module !== 'undefined' && 'exports' in module) module.exports = generarNombres;
