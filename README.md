# Names Generator

Install: `npm install rnamegen`

## Example of use inline 

To generate a name between 4 and 9 letters:
`node node_modules/rnamegen/generate`

To generate 3 names between 5 and 10 letters:
`node node_modules/rnamegen/generate 3 5 10`

## Example of use as node package
```javascript
const generator = require('rnamegen');

const names = generator(3, 4, 9); // 3 names, min 4 letters, max 9 letters
console.log(names);
```

## Javascript Client

See `example.html`

```html
<script type="text/javascript" src="node_modules/rnamegen/dist/namesgen.min.js"></script>
```

## Author

PuppedToy (Alejandro Alcázar)